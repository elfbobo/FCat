package com.xfdmao.fcat.coin.controller;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.xfdmao.fcat.coin.base.entity.KlineInfo;
import com.xfdmao.fcat.coin.base.entity.Matrix;
import com.xfdmao.fcat.coin.base.util.KlineInfoUtil;
import com.xfdmao.fcat.coin.base.util.StrategyUtil;
import com.xfdmao.fcat.coin.entity.Kline;
import com.xfdmao.fcat.coin.huobi.contract.api.HbdmClient;
import com.xfdmao.fcat.coin.huobi.contract.api.HbdmRestApiV1;
import com.xfdmao.fcat.coin.huobi.contract.api.IHbdmRestApi;
import com.xfdmao.fcat.coin.huobi.util.KlineUtil;
import com.xfdmao.fcat.coin.service.KlineService;
import com.xfdmao.fcat.common.controller.BaseController;
import com.xfdmao.fcat.common.util.DateUtil;
import com.xfdmao.fcat.common.util.JsonUtil;
import com.xfdmao.fcat.common.util.StrUtil;
import org.apache.log4j.Logger;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.math.BigDecimal;
import java.util.*;

/**
 * Created by fier on 2018/10/22
 */
@RestController
@RequestMapping("v1/kline")
public class KlineController extends BaseController<KlineService,Kline,Long>{
    private static Logger logger = Logger.getLogger(KlineController.class);

    /**
     * 获取历史K线数据并保存到数据库
     * @return
     */
    @GetMapping(value = "/queryHistoryKlineAndSave")
    public JSONObject queryHistoryKlineAndSave() {
            /**
             * get请求无需发送身份认证,通常用于获取行情，市场深度等公共信息
             */
            IHbdmRestApi futureGetV1 = new HbdmRestApiV1(HbdmClient.url_prex);
            IHbdmRestApi futurePostV1 = new HbdmRestApiV1(HbdmClient.url_prex, "", "");

            String[] coinNames = {"BTC","LTC","ETH","EOS","BSV","BCH","TRX","XRP"};
            String[] periods = {"5min", "15min","60min","4hour","1day"};
            for(int i=0;i<coinNames.length;i++){
                String coinName = coinNames[i];
                for(int j=0;j<periods.length;j++){
                    String period = periods[j];
                    while(true){
                        try{
                            // 获取K线数据
                            String historyKline = futureGetV1.futureMarketHistoryKline(coinName+"_CQ", period,2000+"");
                            logger.info("获取K线数据" + historyKline);
                            JSONObject jsonObject = JSONObject.parseObject(historyKline);
                            JSONArray jsonArray = jsonObject.getJSONArray("data");
                            List<com.xfdmao.fcat.coin.huobi.globle.response.Kline> klines = jsonArray.toJavaList(com.xfdmao.fcat.coin.huobi.globle.response.Kline.class);
                            KlineUtil.sort(klines);
                            klines.remove(0);//移除第一个未收盘的K先
                            List<Kline> kLineList = KlineUtil.toTableKline(klines);
                            for(Kline kline:kLineList){
                                kline.setSymbol(coinName);
                                kline.setContractType("quarter");
                                kline.setPeriod(period);
                            }
                            baseServiceImpl.insertBatch(kLineList);
                            break;
                        }catch (Exception e){
                            e.printStackTrace();
                        }
                    }
                }
            }
        return JsonUtil.getSuccessJsonObject();
    }

    /**
     * 均线策略：
     *      根据指定具体的币种、合约、时间周期、长期均线、短期均线、上引线因子，涨跌因子，买入类型，下跌保护因子，
     *      来计算总收益率和历史交易情况
     * @param symbol
     * @param contractType
     * @param period
     * @param upMaNum
     * @param downMaNum
     * @param upperLeadFactor
     * @param gainFactor
     * @param buyType
     * @param protectionDownFactor
     * @return
     */
    @GetMapping(value = "/queryTwoMaStrategy")
    public JSONObject queryTwoMaStrategy( @RequestParam("symbol") String symbol, @RequestParam("contractType") String contractType,
                                           @RequestParam("period") String period ,@RequestParam("upMaNum") Integer upMaNum ,@RequestParam("downMaNum") Integer downMaNum,
                                           @RequestParam("upperLeadFactor") double upperLeadFactor,
                                           @RequestParam("gainFactor") double gainFactor,
                                          @RequestParam("buyType") String buyType,
                                          @RequestParam("protectionDownFactor") double protectionDownFactor,
                                          @RequestParam(value="volFactory", required = false,defaultValue="2.5") double volFactory,
                                          @RequestParam(value="upperLeadGain", required = false,defaultValue="0.0072") double upperLeadGain
                                        ){
        List<Kline> kLineList = baseServiceImpl.queryList(symbol,contractType,period);
        List<KlineInfo> kLines = KlineInfoUtil.getKLines(kLineList);
        List<KlineInfo> result = StrategyUtil.strategyTwoAvg(kLines,upMaNum,downMaNum,symbol,upperLeadFactor,gainFactor,protectionDownFactor,volFactory,upperLeadGain);
        Collections.reverse(result);
        KlineInfoUtil.dealSumIncomeRateList(result,buyType);
        KlineInfoUtil.print(result,upMaNum,KlineInfoUtil.getAvg(kLines,upMaNum),downMaNum,KlineInfoUtil.getAvg(kLines,downMaNum));
        double income = KlineInfoUtil.getSumIncomeRateRealList(result);
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("income",income*100);
        jsonObject.put("sumNum",result.size());
        JSONArray jsonArray = new JSONArray();
        for(int i=0;i<result.size();i++){
            KlineInfo klineInfo = result.get(i);
            JSONObject jsonObject1 = JSONObject.parseObject(JSONObject.toJSONString(klineInfo));
            jsonObject1.put("date", DateUtil.formatDate(klineInfo.getDate(),DateUtil.TIME_PATTERN_DISPLAY) );
            jsonArray.add(jsonObject1);
        }
        jsonObject.put("detail",jsonArray);

        JSONArray klinesArr = new JSONArray();
        for(int i=0;i<kLines.size();i++){
            KlineInfo klineInfo = kLines.get(i);
            for(KlineInfo klineInfo1:result){
                if(klineInfo.getDate().getTime() == klineInfo1.getDate().getTime()){
                    klineInfo.setBuySellStatus(klineInfo1.getBuySellStatus());
                    klineInfo.setIncomeRate(klineInfo1.getIncomeRate());
                    break;
                }
            }
            if(StrUtil.isBlank(klineInfo.getBuySellStatus())){
                klineInfo.setBuySellStatus("");
                klineInfo.setIncomeRate(0);
            }
            JSONObject jsonObject1 = JSONObject.parseObject(JSONObject.toJSONString(klineInfo));
            jsonObject1.put("date", DateUtil.formatDate(klineInfo.getDate(),DateUtil.TIME_PATTERN_DISPLAY) );
            klinesArr.add(jsonObject1);
        }

        double incomeRealKlines = KlineInfoUtil.getSumIncomeRateRealList(kLines);
        jsonObject.put("incomeKlines",incomeRealKlines*100);
        jsonObject.put("kLines",klinesArr);

        return JsonUtil.getSuccessJsonObject(jsonObject);
    }

    /**
     * 均线策略：做多
     *      获取长期均线和短期均线的矩阵收益值，返回二位数组的对象列表
     * @param symbol
     * @param contractType
     * @param period
     * @param maxMA
     * @param upperLeadFactor
     * @param gainFactor
     * @param buyType
     * @param protectionDownFactor
     * @return
     */
    @GetMapping(value = "/queryTwoMaStrategyBest")
    public JSONObject queryTwoMaStrategyBest( @RequestParam("symbol") String symbol, @RequestParam("contractType") String contractType,
                                              @RequestParam("period") String period ,@RequestParam("maxMA") Integer maxMA,
                                              @RequestParam("upperLeadFactor") double upperLeadFactor,
                                              @RequestParam("gainFactor") double gainFactor,
                                              @RequestParam("buyType") String buyType,
                                              @RequestParam("protectionDownFactor") double protectionDownFactor,
                                              @RequestParam(value="volFactory", required = false,defaultValue="2.5") double volFactory,
                                              @RequestParam(value="upperLeadGain", required = false,defaultValue="0.0072") double upperLeadGain){
        List<Kline> kLineList = baseServiceImpl.queryList(symbol,contractType,period);
        List<KlineInfo> kLines = KlineInfoUtil.getKLines(kLineList);
        List<Matrix> matrixs = StrategyUtil.queryTwoMaStrategyBest(kLines,maxMA,symbol,upperLeadFactor,gainFactor,buyType,protectionDownFactor,volFactory,upperLeadGain);
        JSONArray result = new JSONArray();
        for(int i=0;i<matrixs.size();i++){
            JSONObject jsonObject = JSONObject.parseObject(JSONObject.toJSONString(matrixs.get(i)));
            jsonObject.put("value",new BigDecimal(jsonObject.getDouble("value")).multiply(new BigDecimal(100)).setScale(2,BigDecimal.ROUND_DOWN));
            result.add(jsonObject);
        }
        return JsonUtil.getSuccessJsonObject(result);
    }

    /**
     * 均线策略：做空
     *      获取长期均线和短期均线的矩阵收益值，返回二位数组的对象列表
     * @param symbol
     * @param contractType
     * @param period
     * @param maxMA
     * @param upperLeadFactor
     * @param gainFactor
     * @param buyType
     * @param protectionDownFactor
     * @return
     */
    @GetMapping(value = "/queryTwoMaStrategySellBest")
    public JSONObject queryTwoMaStrategySellBest( @RequestParam("symbol") String symbol, @RequestParam("contractType") String contractType,
                                              @RequestParam("period") String period ,@RequestParam("maxMA") Integer maxMA,
                                              @RequestParam("upperLeadFactor") double upperLeadFactor,
                                              @RequestParam("gainFactor") double gainFactor,
                                              @RequestParam("buyType") String buyType,
                                              @RequestParam("protectionDownFactor") double protectionDownFactor){
        List<Kline> kLineList = baseServiceImpl.queryList(symbol,contractType,period);
        List<KlineInfo> kLines = KlineInfoUtil.getKLines(kLineList);
        List<Matrix> matrixs = StrategyUtil.queryTwoMaStrategySellBest(kLines,maxMA,symbol,upperLeadFactor,gainFactor,buyType,protectionDownFactor);
        JSONArray result = new JSONArray();
        for(int i=0;i<matrixs.size();i++){
            JSONObject jsonObject = JSONObject.parseObject(JSONObject.toJSONString(matrixs.get(i)));
            jsonObject.put("value",new BigDecimal(jsonObject.getDouble("value")).multiply(new BigDecimal(100)).setScale(2,BigDecimal.ROUND_DOWN));
            result.add(jsonObject);
        }
        return JsonUtil.getSuccessJsonObject(result);
    }

    /**
     * 均线策略：做空
     *      根据指定具体的币种、合约、时间周期、长期均线、短期均线、上引线因子，涨跌因子，买入类型，下跌保护因子，
     *      来计算总收益率和历史交易情况
     * @param symbol
     * @param contractType
     * @param period
     * @param upMaNum
     * @param downMaNum
     * @param upperLeadFactor
     * @param gainFactor
     * @param buyType
     * @param protectionDownFactor
     * @return
     */
    @GetMapping(value = "/queryTwoMaStrategySell")
    public JSONObject queryTwoMaStrategySell( @RequestParam("symbol") String symbol, @RequestParam("contractType") String contractType,
                                          @RequestParam("period") String period ,@RequestParam("upMaNum") Integer upMaNum ,@RequestParam("downMaNum") Integer downMaNum,
                                          @RequestParam("upperLeadFactor") double upperLeadFactor,
                                          @RequestParam("gainFactor") double gainFactor,
                                          @RequestParam("buyType") String buyType,
                                          @RequestParam("protectionDownFactor") double protectionDownFactor
    ){
        List<Kline> kLineList = baseServiceImpl.queryList(symbol,contractType,period);
        List<KlineInfo> kLines = KlineInfoUtil.getKLines(kLineList);
        List<KlineInfo> result = StrategyUtil.strategySellTwoAvg(kLines,upMaNum,downMaNum,symbol,upperLeadFactor,gainFactor,protectionDownFactor);
        KlineInfoUtil.dealSumIncomeRateList(result,buyType);
        KlineInfoUtil.print(result,upMaNum,KlineInfoUtil.getAvg(kLines,upMaNum),downMaNum,KlineInfoUtil.getAvg(kLines,downMaNum));
        double income = KlineInfoUtil.getSumIncomeRateRealList(result);
        Collections.reverse(result);
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("income",income*100);
        jsonObject.put("sumNum",result.size());
        JSONArray jsonArray = new JSONArray();
        for(int i=0;i<result.size();i++){
            KlineInfo klineInfo = result.get(i);
            JSONObject jsonObject1 = JSONObject.parseObject(JSONObject.toJSONString(klineInfo));
            jsonObject1.put("date", DateUtil.formatDate(klineInfo.getDate(),DateUtil.TIME_PATTERN_DISPLAY) );
            jsonArray.add(jsonObject1);
        }
        jsonObject.put("detail",jsonArray);

        JSONArray klinesArr = new JSONArray();
        for(int i=0;i<kLines.size();i++){
            KlineInfo klineInfo = kLines.get(i);
            for(KlineInfo klineInfo1:result){
                if(klineInfo.getDate().getTime() == klineInfo1.getDate().getTime()){
                    klineInfo.setBuySellStatus(klineInfo1.getBuySellStatus());
                    klineInfo.setIncomeRate(klineInfo1.getIncomeRate());
                    break;
                }
            }
            if(StrUtil.isBlank(klineInfo.getBuySellStatus())){
                klineInfo.setBuySellStatus("");
                klineInfo.setIncomeRate(0);
            }
            JSONObject jsonObject1 = JSONObject.parseObject(JSONObject.toJSONString(klineInfo));
            jsonObject1.put("date", DateUtil.formatDate(klineInfo.getDate(),DateUtil.TIME_PATTERN_DISPLAY) );
            klinesArr.add(jsonObject1);
        }

        double incomeRealKlines = KlineInfoUtil.getSumIncomeRateRealList(kLines);
        jsonObject.put("incomeKlines",incomeRealKlines*100);
        jsonObject.put("kLines",klinesArr);

        return JsonUtil.getSuccessJsonObject(jsonObject);
    }


    /**
     * 均线策略：
     *      关键K线策略
     * @param symbol
     * @param contractType
     * @param period
     * @param upMaNum
     * @param downMaNum
     * @param upperLeadFactor
     * @param gainFactor
     * @param buyType
     * @param protectionDownFactor
     * @return
     */
        @GetMapping(value = "/querykeyKlineStrategy")
    public JSONObject querykeyKlineStrategy( @RequestParam("symbol") String symbol, @RequestParam("contractType") String contractType,
                                          @RequestParam("period") String period ,@RequestParam("upMaNum") Integer upMaNum ,@RequestParam("downMaNum") Integer downMaNum,
                                          @RequestParam("upperLeadFactor") double upperLeadFactor,
                                          @RequestParam("gainFactor") double gainFactor,
                                          @RequestParam("buyType") String buyType,
                                          @RequestParam("protectionDownFactor") double protectionDownFactor,
                                          @RequestParam(value="volFactory", required = false,defaultValue="2.5") double volFactory,
                                          @RequestParam(value="upperLeadGain", required = false,defaultValue="0.0072") double upperLeadGain
    ){
        List<Kline> kLineList = baseServiceImpl.queryList(symbol,contractType,period);
        List<KlineInfo> kLines = KlineInfoUtil.getKLines(kLineList);
        List<KlineInfo> result = StrategyUtil.strategykeyKline(kLines,upMaNum,downMaNum,symbol,upperLeadFactor,gainFactor,protectionDownFactor,volFactory,upperLeadGain);
        Collections.reverse(result);
        KlineInfoUtil.dealSumIncomeRateList(result,buyType);
        KlineInfoUtil.print(result,upMaNum,KlineInfoUtil.getAvg(kLines,upMaNum),downMaNum,KlineInfoUtil.getAvg(kLines,downMaNum));
        double income = KlineInfoUtil.getSumIncomeRateRealList(result);
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("income",income*100);
        jsonObject.put("sumNum",result.size());
        JSONArray jsonArray = new JSONArray();
        for(int i=0;i<result.size();i++){
            KlineInfo klineInfo = result.get(i);
            JSONObject jsonObject1 = JSONObject.parseObject(JSONObject.toJSONString(klineInfo));
            jsonObject1.put("date", DateUtil.formatDate(klineInfo.getDate(),DateUtil.TIME_PATTERN_DISPLAY) );
            jsonArray.add(jsonObject1);
        }
        jsonObject.put("detail",jsonArray);

        JSONArray klinesArr = new JSONArray();
        for(int i=0;i<kLines.size();i++){
            KlineInfo klineInfo = kLines.get(i);
            for(KlineInfo klineInfo1:result){
                if(klineInfo.getDate().getTime() == klineInfo1.getDate().getTime()){
                    klineInfo.setBuySellStatus(klineInfo1.getBuySellStatus());
                    klineInfo.setIncomeRate(klineInfo1.getIncomeRate());
                    break;
                }
            }
            if(StrUtil.isBlank(klineInfo.getBuySellStatus())){
                klineInfo.setBuySellStatus("");
                klineInfo.setIncomeRate(0);
            }
            JSONObject jsonObject1 = JSONObject.parseObject(JSONObject.toJSONString(klineInfo));
            jsonObject1.put("date", DateUtil.formatDate(klineInfo.getDate(),DateUtil.TIME_PATTERN_DISPLAY) );
            klinesArr.add(jsonObject1);
        }

        double incomeRealKlines = KlineInfoUtil.getSumIncomeRateRealList(kLines);
        jsonObject.put("incomeKlines",incomeRealKlines*100);
        jsonObject.put("kLines",klinesArr);

        return JsonUtil.getSuccessJsonObject(jsonObject);
    }
}
