package com.xfdmao.fcat.coin.controller;

import com.xfdmao.fcat.coin.entity.StrategyParam;
import com.xfdmao.fcat.coin.entity.Token;
import com.xfdmao.fcat.coin.huobi.util.StrategyUtil;
import com.xfdmao.fcat.coin.service.StrategyParamService;
import com.xfdmao.fcat.coin.service.TokenService;
import org.apache.http.HttpException;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;

import java.io.IOException;
import java.util.List;

/**
 * Created by fier on 2018/10/31.
 */
@Controller
public class TransactionController {
    private static Logger logger = Logger.getLogger(TransactionController.class);
    @Autowired
    private StrategyParamService strategyParamService;


    @Value("${spring.profiles.active}")
    private String active;

    @Autowired
    private TokenService tokenService;

    public void towAvg() {
        if (!"transaction".equals(active)) return;
        while (true) {
            logger.info("\n\n\n"); 
            try {
                List<StrategyParam> strategyParamList = strategyParamService.selectListAll();
                List<Token> tokens = tokenService.selectListAll();
                for (Token token : tokens) {
                    if (token.getTestEnable()) continue;//过滤测试账号
                    logger.info("///////////////////////" + token.getUsername() + " start///////////////////////");
                    for (StrategyParam strategyParam : strategyParamList) {
                        if(!strategyParam.getEnable())continue;
                        if (strategyParam.getUsername().equals(token.getUsername())) {
                            logger.info("\n");
                            if ("EOS".equals(strategyParam.getSymbol())) {
                                if ("buy".equals(strategyParam.getBuySell())) {
                                    StrategyUtil.towMaEOS(strategyParam, token);
                                } else if ("sell".equals(strategyParam.getBuySell())) {
                                    StrategyUtil.towMaEOSAndBTCSell(strategyParam, token);
                                }
                            } else if ("BTC".equals(strategyParam.getSymbol())) {
                                if ("buy".equals(strategyParam.getBuySell())) {
                                    StrategyUtil.towMaBTC(strategyParam, token);
                                }else if("sell".equals(strategyParam.getBuySell())){
                                    StrategyUtil.towMaEOSAndBTCSell(strategyParam, token);
                                }
                            }
                        }
                    }
                    logger.info("|||||||||||||||||||||| = " + token.getUsername() + " end = ||||||||||||||||||||||\n");
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}