package com.xfdmao.fcat.coin.mapper;

import com.xfdmao.fcat.coin.entity.Aicoin;
import tk.mybatis.mapper.common.Mapper;

public interface AicoinMapper extends Mapper<Aicoin> {
}