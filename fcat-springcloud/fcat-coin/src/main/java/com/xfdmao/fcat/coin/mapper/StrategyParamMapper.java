package com.xfdmao.fcat.coin.mapper;

import com.xfdmao.fcat.coin.entity.StrategyParam;
import tk.mybatis.mapper.common.Mapper;

public interface StrategyParamMapper extends Mapper<StrategyParam> {
}